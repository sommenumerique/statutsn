---
section: issue
title: Maintenance accès FTTO et FTTH
date: 2024-10-22T00:00:00.000Z
status: resolved
pinned: ""
current_severity: down
max_severity: maintenance
duration: 6 heures
resolved_on: 2024-10-22T02:00:00.000Z
affected:
  - Accès Internet Fibre
twitterFeed: ""
enableComments: false
---
Une mise à jour des équipements de collecte du gestionnaire du réseau (AI Somme) va impacter l'ensemble des services d'accès du GFU Somme Numérique à partir de 0h01 le 22/10/2024.

L'intervention est prévue de durer 1h et devrait être terminée avant 6h.
