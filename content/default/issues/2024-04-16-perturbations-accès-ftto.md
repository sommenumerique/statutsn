---
section: issue
title: Perturbations accès FTTO
date: 2024-04-16T09:03:00.000Z
status: resolved
pinned: ""
current_severity: ok
max_severity: disrupted
duration: ""
resolved_on: 2024-04-16T11:24:00.000Z
affected:
  - Accès Internet Fibre
twitterFeed: ""
enableComments: false
---
Suite à l'opération de maintenance ayant eu lieu cette nuit, certains sites fibrés Somme Numérique rencontrent des difficultés d'accès à Internet. Nos équipes sont en cours d'investigation.

—

11h30 - L'incident est résolu. Nous restons en surveillance.
