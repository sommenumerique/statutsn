---
section: issue
title: Maintenance transit Internet
date: 2024-06-10T22:00:00.000Z
status: resolved
pinned: ""
current_severity: maintenance
max_severity: disrupted
duration: 2h
resolved_on: 2024-06-11T09:30:00.000Z
affected:
  - Accès Internet Fibre
twitterFeed: ""
enableComments: false
---
Nous devons intervenir dans les prochains jours pour finaliser une opération très importante de migration de notre cœur de réseau fibre optique.

Cela va se matérialiser par le déménagement d'un routeur, occasionnant une coupure franche de l'accès Internet très haut débit des sites publics connectés à notre réseau de fibre optique, et donc la perte de tous les services associés.

Le présent message constitue une prévenance pour vous informer de la réalisation de cette opération dans la nuit du 10 au 11 juin, voire dans la nuit du 17 au 18 juin.

Cette intervention est programmée de nuit, afin de limiter l'impact sur nos membres.

Cependant, en cas de difficulté sur la reprise de l'équipement déménagé, nous vous demandons toute votre vigilance et vous recommandons de nous signaler toute anomalie relevée sur vos services dans les prochains jours.

Nous ne manquerons pas de vous tenir informés des suites de ces opérations, en espérant un succès sans incident.

Nous ne pouvons pas échapper à ces dernières interventions, qui ont pour objectif de renforcer la sécurisation de notre réseau et la continuité des services mis en place pour nos membres.

Je vous remercie pour votre compréhension



\----

**23h30** Fin de l'intervention



**11 janvier 9h30**

En conséquence de la maintenance effectuée le 10/06, certains accès sont restés perturbés pendant la nuit.

L'ensemble est à nouveau fonctionnel depuis 9h15.
