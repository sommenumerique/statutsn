---
section: issue
title: Maintenance planifiée hébergements Web
date: 2024-09-26T18:00:00.000Z
status: resolved
pinned: ""
current_severity: ok
max_severity: maintenance
duration: 1h
resolved_on: 2024-09-26T18:44:00.000Z
affected:
  - Sites Internet Wordpress
twitterFeed: ""
enableComments: false
---
Une plage de maintenance est planifiée jeudi 26 septembre à partir de 18h.

Des coupures d'accès aux sites Internet hébergés pourront survenir durant la période de maintenance.

—

**18:00** : début de l'opération de mise à niveau.

**18:30** : les opérations de mise à jour se poursuivent normalement.

**18:44** : la maintenance est terminée.
