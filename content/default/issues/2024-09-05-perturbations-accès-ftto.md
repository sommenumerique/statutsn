---
section: issue
title: Perturbations accès FTTO
date: 2024-09-05T12:10:00.000Z
status: resolved
pinned: ""
current_severity: ok
max_severity: down
duration: ""
resolved_on: 2024-09-05T12:30:00.000Z
affected:
  - Accès Internet Fibre
twitterFeed: ""
enableComments: false
---
Un incident sur le parefeu mutualisé des accès « GFU » a provoqué une indidsponibilité des accès internet pendant une vingtaine de minutes.
