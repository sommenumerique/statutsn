---
section: issue
title: Perturbation des accès internet et services hébergés
date: 2024-08-20T08:05:00.000Z
status: resolved
pinned: ""
current_severity: monitoring
max_severity: down
duration: ""
resolved_on: 2024-08-20T08:25:00.000Z
affected:
  - Accès Internet Fibre
  - Sites Internet Wordpress
  - Messagerie électronique
twitterFeed: ""
enableComments: false
---
Un défaut chez les fournisseurs de transit IP de Somme Numérique a entrainé une dégradation des services à partir de 10h05, engendrant des pertes de paquet et des latences élevées.



**10 h 25** - les services semblent à nouveau opérationnels
