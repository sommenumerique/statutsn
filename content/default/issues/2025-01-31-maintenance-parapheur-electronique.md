---
section: issue
title: Maintenance Parapheur Electronique
date: 2025-01-31T17:30:00.000Z
status: resolved
pinned: null
current_severity: disrupted
max_severity: disrupted
duration: ""
resolved_on: 2025-01-31T19:00:00.000Z
affected:
  - Parapheur électronique
twitterFeed: ""
enableComments: false
---
Afin de réindexer les données du Parapheur Électronique de Somme Numérique ( https://parapheur.sommenumerique.fr/ ) une opération de maintenance est programmée le 31 janvier à  partir de 17h30.
