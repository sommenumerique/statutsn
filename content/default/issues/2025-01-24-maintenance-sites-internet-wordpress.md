---
section: issue
title: Maintenance Sites Internet Wordpress
date: 2025-01-27T18:00:00.000Z
status: resolved
pinned: ""
current_severity: maintenance
max_severity: maintenance
duration: 10m
resolved_on: 2025-01-27T18:10:00.000Z
affected:
  - Sites Internet Wordpress
twitterFeed: ""
enableComments: false
---
Une interruption de quelques minutes est à prévoir sur l'hébergement mutualisé des sites Wordpress.
