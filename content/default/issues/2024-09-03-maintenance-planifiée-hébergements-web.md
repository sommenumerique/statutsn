---
section: issue
title: Maintenance planifiée hébergements Web
date: 2024-09-12T18:00:00.000Z
status: resolved
pinned: ""
current_severity: ok
max_severity: maintenance
duration: 30m
resolved_on: 2024-09-12T18:58:00.000Z
affected:
  - Sites Internet Wordpress
twitterFeed: ""
enableComments: false
---
Une plage de maintenance est planifiée jeudi 12 septembre à partir de 18h.

Des coupures d'accès aux sites Internet hébergés pourront survenir durant la période de maintenance.

—

**18h02** : Nous démarrons la mise à niveau.

**18h58** : La mise à niveau est terminée. L'ensemble des opérations souhaitées n'ont pas pu toutes être menées, une nouvelle date sera replanifée.
