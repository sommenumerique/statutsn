---
section: issue
title: Maintenance des accès FFTH
date: 2024-04-18T22:00:00.000Z
status: resolved
pinned: ""
current_severity: ok
max_severity: maintenance
duration: 8h
resolved_on: 2024-04-19T06:00:00.000Z
affected:
  - Accès Internet Fibre
twitterFeed: ""
enableComments: false
---
Dans le cadre d'une opération de maintenance votre accès fibre fourni par Somme Numérique pourra être indisponible de 22h00 à 6h00.
