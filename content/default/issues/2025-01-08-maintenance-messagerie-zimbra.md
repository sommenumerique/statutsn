---
section: issue
title: Maintenance messagerie Zimbra
date: 2025-01-13T13:00:00.000Z
status: resolved
pinned: ""
current_severity: maintenance
max_severity: maintenance
duration: 1h
resolved_on: 2025-01-13T14:00:00.000Z
affected:
  - Messagerie électronique
twitterFeed: ""
enableComments: false
---
La messagerie Zimbra sera inaccessible entre 13 h 00 et 14 h 00 pour maintenance. 

13:30 La maintenance est toujours en cours.

13:55 La maintenance est terminée.
