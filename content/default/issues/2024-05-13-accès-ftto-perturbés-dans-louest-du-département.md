---
section: issue
title: Accès FTTO perturbés dans l'ouest du département
date: 2024-05-13T15:45:00.000Z
status: resolved
pinned: ""
current_severity: ok
max_severity: disrupted
duration: ""
resolved_on: 2024-05-13T16:50:00.000Z
affected:
  - Accès Internet Fibre
twitterFeed: ""
enableComments: false
---
Les services d'accès sur fibre optique FTTO fournis dans l'ouest du département ( de la CC des villes soeurs au Vimeu ) sont perturbés suite à un probable incident générique.

**16h50** Un nouvel équipement mis en oeuvre par le délégataire du réseau Somme Numérique a provoqué les perturbations constatées.
