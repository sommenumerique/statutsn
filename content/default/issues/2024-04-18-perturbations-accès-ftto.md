---
section: issue
title: Perturbations accès FTTO
date: 2024-04-18T15:51:00.000Z
status: resolved
pinned: ""
current_severity: disrupted
max_severity: disrupted
duration: ""
resolved_on: 2024-04-22T09:00:00.000Z
affected:
  - Accès Internet Fibre
twitterFeed: ""
enableComments: false
---
Nous rencontrons actuellement des perturbations sur certains accès Fibre Optique dédiés pouvant entraîner une perte de connexion. Nos équipes sont en cours d'investigation sur cet incident.
