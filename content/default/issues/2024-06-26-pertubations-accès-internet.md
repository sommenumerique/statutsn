---
section: issue
title: Pertubations accès Internet
date: 2024-06-26T08:30:00.000Z
status: resolved
pinned: ""
current_severity: disrupted
max_severity: disrupted
duration: ""
resolved_on: 2024-06-26T10:50:00.000Z
affected:
  - Accès Internet Fibre
twitterFeed: ""
enableComments: false
---
Certains accès Internet ne sont actuellement pas fonctionnel. Nous sommes en train d'investiguer.

—

10h10 : Le problème est identifié et en cours de résolution, nous observons un début de remontée des services.

—

10h50 : Le problème est résolu, les services sont remontés. Si ce n'est pas votre cas, merci de nous contacter.
