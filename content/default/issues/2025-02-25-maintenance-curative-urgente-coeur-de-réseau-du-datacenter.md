---
section: issue
title: Maintenance curative urgente coeur de réseau du datacenter
date: 2025-02-25T12:00:00.000Z
status: resolved
pinned: ""
current_severity: maintenance
max_severity: disrupted
duration: ""
resolved_on: 2025-02-25T12:40:00.000Z
affected:
  - Accès Internet Fibre
  - Messagerie électronique
  - Dématérialisation de flux
  - Plateforme Démarches Simplifiées
  - Plateforme Données Citoyennes
  - Parapheur électronique
  - Noms de domaines
  - Portail Iris
  - Pages Mairies Connectées
  - Sites Internet Wordpress
twitterFeed: ""
enableComments: false
---
Une intervention urgente est nécessaire sur un switch de coeur de réseau du datacenter, nécessitant son redémarrage.

La maintenance est planifiée le 25/02/205 à partir de midi.

L'interventation provoquera des perturbations sur la disponibilité de l'ensemble des services. Aucune coupure complète n'est prévue.
