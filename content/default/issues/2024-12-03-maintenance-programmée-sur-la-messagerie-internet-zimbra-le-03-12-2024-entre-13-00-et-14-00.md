---
section: issue
title: Maintenance programmée sur la messagerie internet Zimbra le
  03/12/2024   entre 13:00 et 14:00
date: 2024-12-03T13:00:00.000Z
status: resolved
pinned: ""
current_severity: down
max_severity: down
duration: 1 heure
resolved_on: 2024-12-03T14:00:00.000Z
affected:
  - Messagerie électronique
twitterFeed: ""
enableComments: false
---
