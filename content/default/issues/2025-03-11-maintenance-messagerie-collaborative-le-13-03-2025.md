---
section: issue
title: Maintenance Messagerie Collaborative le 13/03/2025
date: 2025-03-13T13:00:00.000Z
status: scheduled
pinned: belowheader
current_severity: maintenance
max_severity: disrupted
duration: 2h
affected:
  - Messagerie électronique
twitterFeed: ""
enableComments: false
---
Une intervention sur la messagerie Zimbra sera réalisée le jeudi 13 mars de 12h00 à 14h00 afin de mettre en place un mécanisme permettant d'aiguiller les utilisateurs sur la version actuelle de la messagerie ou vers la nouvelle version (Carbonio).

Cette intervention pourrait entraîner quelques perturbations d'accès ou d'usage.

Si un souci majeur était detecté, nous ferions alors un retour en arrière.
