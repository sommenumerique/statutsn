---
section: issue
title: Perturbation accès FTTH
date: 2024-11-07T09:00:00.000Z
status: resolved
pinned: ""
current_severity: ok
max_severity: disrupted
duration: 5h
resolved_on: 2024-11-07T14:15:00.000Z
affected:
  - Accès Internet Fibre
twitterFeed: ""
enableComments: false
---
Certains membres fibrés en FTTH peuvent rencontrer des problématiques d'accès à Internet. Nos équipes sont en cours d'investigation sur cet incident.

—

**14:15** : l'origine de l'incident a été identifiée et une correction a été effectuée. Les différents accès FTTH sont de nouveau opérationnels.
