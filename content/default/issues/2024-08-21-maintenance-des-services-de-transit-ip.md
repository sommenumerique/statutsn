---
section: issue
title: Maintenance des services de transit IP
date: 2024-08-22T21:00:00.000Z
status: resolved
pinned: ""
current_severity: maintenance
max_severity: maintenance
duration: 2h
resolved_on: 2024-08-22T22:30:00.000Z
affected:
  - Accès Internet Fibre
  - Messagerie électronique
  - Noms de domaines
  - Portail Iris
  - Parapheur électronique
  - Dématérialisation de flux
  - Sites Internet Wordpress
  - Pages Mairies Connectées
  - Plateforme Données Citoyennes
  - Plateforme Démarches Simplifiées
twitterFeed: ""
enableComments: false
---
Afin de mettre en service un nouvel opérateur de transit IP, une maintenance est programmée sur les services de routage de la plateforme d'accès internet et d'hébergement de Somme Numérique le 22 aout 2024, à partir de 21h.

Les opérations programmées ne devraient pas entrainer de perturbations majeures du services, mais des pertes de paquets ponctuels sont possibles pour l'ensemble des accès concernés.

\====

**21 h** - Début des opérations

**22 h 30** - Fin de la maintenance.
