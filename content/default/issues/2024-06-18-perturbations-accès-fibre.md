---
section: issue
title: Perturbations accès fibre
date: 2024-06-18T08:00:00.000Z
status: resolved
pinned: ""
current_severity: ok
max_severity: disrupted
duration: 50m
resolved_on: 2024-06-18T08:50:00.000Z
affected:
  - Accès Internet Fibre
twitterFeed: ""
enableComments: false
---
Suite à l'intervention de maintenance programmée ayant eu lieu cette nuit, certains membres peuvent rencontrer des difficultés d'accès à Internet. Nos équipes sont en cours d'investigation sur cet incident. Nous posterons davantage de détails dès que nous en aurons.

—

**8 h 50** : Les accès sont de nouveau opérationnels.
