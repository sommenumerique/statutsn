---
section: issue
title: Maintenance messagerie électronique
date: 2024-11-27T13:00:00.000Z
status: resolved
pinned: ""
current_severity: maintenance
max_severity: maintenance
duration: ""
resolved_on: 2024-11-27T14:20:00.000Z
affected:
  - Messagerie électronique
twitterFeed: ""
enableComments: false
---
Une maintenance est prévue sur le service de messagerie électronique le 27/11 de 13 h à 14 h.

—

**14 h 15** : la maintenance est plus longue que prévu suite à un problème.

—

**14 h 20** : la cause du problème est identifié, cependant le créneau étant terminé, le reste de la maintenance sera réalisé à une date ultérieure.
