---
section: issue
title: Mise à jour du certificat SSL sur messagerie.sommenumerique.fr
date: 2024-04-25T12:50:00.000Z
status: resolved
pinned: null
current_severity: ok
max_severity: maintenance
duration: 10mn
resolved_on: 2024-04-23T13:50:00.000Z
affected:
  - Messagerie électronique
twitterFeed: ""
enableComments: false
---
Planifié : le prestataire en charge de maintenir la solution de messagerie Zimbra procédera le 25/04 à 13h00 au renouvellement du certificat SSL. Une interruption de 10 min. max par mailstore est à prévoir.
