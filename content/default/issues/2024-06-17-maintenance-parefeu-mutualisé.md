---
section: issue
title: Maintenance parefeu mutualisé
date: 2024-06-17T21:00:00.000Z
status: resolved
pinned: ""
current_severity: maintenance
max_severity: maintenance
duration: ""
resolved_on: 2024-06-17T23:00:00.000Z
affected:
  - Accès Internet Fibre
  - Messagerie électronique
  - Plateforme Démarches Simplifiées
  - Plateforme Données Citoyennes
  - Pages Mairies Connectées
  - Sites Internet Wordpress
  - Parapheur électronique
  - Noms de domaines
  - Portail Iris
  - Dématérialisation de flux
twitterFeed: ""
enableComments: false
---
Afin de finaliser le transfert des services vers le nouveau pop « Italie », une maintenance est programmée le 17 juin 2024 à partir de 21H.
