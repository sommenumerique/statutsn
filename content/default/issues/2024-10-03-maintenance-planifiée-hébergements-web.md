---
section: issue
title: Maintenance planifiée hébergements Web
date: 2024-10-03T18:00:00.000Z
status: resolved
pinned: ""
current_severity: ok
max_severity: maintenance
duration: 1h
resolved_on: 2024-10-03T18:20:00.000Z
affected:
  - Sites Internet Wordpress
twitterFeed: ""
enableComments: false
---
Une plage de maintenance est planifiée jeudi 03 octobre à partir de 18h.

Des coupures d'accès aux sites Internet hébergés pourront survenir durant la période de maintenance.

—

**18:00** : début de l'opération de mise à niveau.

**18:20** : l'opération de maintenance s'est terminée avec succès.
