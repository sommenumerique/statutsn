---
section: issue
title: Intervention planifiée Pastell
date: 2025-01-17T10:00:00.000Z
status: resolved
pinned: ""
current_severity: maintenance
max_severity: maintenance
duration: ""
resolved_on: 2025-01-17T10:30:00.000Z
affected:
  - Dématérialisation de flux
twitterFeed: ""
enableComments: false
---
Une intervention sera réalisée sur Pastell le 17/01 à 10h00.

Cette intervention ne devrait pas occasionner d'interruption de service.
