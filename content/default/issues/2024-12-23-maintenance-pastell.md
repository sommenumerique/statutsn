---
section: issue
title: Maintenance Pastell
date: 2024-12-23T07:59:00.000Z
status: resolved
pinned: null
current_severity: disrupted
max_severity: down
duration: ""
resolved_on: 2024-12-23T08:02:00.000Z
affected:
  - Dématérialisation de flux
  - Portail Iris
twitterFeed: ""
enableComments: false
---
Nous rencontrons un incident sur la plateforme Pastell (actes, PES). Notre équipe est en train de solutionner le souci, cela ne devrait durer que quelques minutes. Merci de votre patience.
